########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship, backref
from qops_tablet import db


from ..mod_location.models import Location


class Base(db.Model):
    __abstract__ = True
    __bind_key__ = 'qops'

    id = db.Column(db.Integer, primary_key=True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime,
                           default=db.func.now(),
                           onupdate=db.func.now())
    date_onboard = db.Column(db.DateTime, default=db.func.now())
    date_deboard = db.Column(db.DateTime)


class IndividualCustomer(Base):
    __tablename__ = 'individual_customer'

    first_name = db.Column(db.Unicode)
    middle_name = db.Column(db.Unicode)
    last_name = db.Column(db.Unicode)
    date_of_birth = db.Column(db.DateTime)
    date_of_death = db.Column(db.DateTime)


class IndividualCustomerLocation(Base):
    __tablename__ = 'individual_customer_location'

    customer_id = db.Column(db.Integer,
                            db.ForeignKey('individual_customer_location.id'))
    location_id = db.Column(db.Integer,
                            db.ForeignKey('location.id'))


class ForProfitOrgCustomer(Base):
    __tablename__ = 'for_profit_org_customer'

    name = db.Column(db.String(72))


class ForProfitOrgCustomerLocation(Base):
    __tablename__ = 'for_profit_org_customer_location'

    customer_id = db.Column(db.Integer,
                            db.ForeignKey('for_profit_org_customer.id'))
    location_id = db.Column(db.Integer,
                            db.ForeignKey('location.id'))


class NotForProfitOrgCustomer(Base):
    __tablename__ = 'not_for_profit_org_customer'

    name = db.Column(db.String(72))


class NotForProfitOrgCustomerLocation(Base):
    __tablename__ = 'not_for_profit_org_customer_location'

    customer_id = db.Column(db.Integer,
                            db.ForeignKey('not_for_profit_org_customer.id'))
    location_id = db.Column(db.Integer,
                            db.ForeignKey('location.id'))
